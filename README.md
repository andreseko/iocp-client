IOCP Client C#
=====================

## How to use?

```
using System;
using IOCPClient;
using System.Collections.Generic;
using System.Timers;

namespace TestIOCP
{
	public class TestConnection
	{
		private IOCP iocpCli;
		public TestConnection ()
		{
			iocpCli = new IOCP ("192.168.25.12");
			Dictionary <string, int> maps = new Dictionary<string, int> ();
			maps.Add ("sw_mode_auto", 10);
			maps.Add ("sw_test", 11);
			maps.Add ("sw_erase", 12);
			iocpCli.setMapping (maps);

			// Monitor connections
			Timer monitorConnection = new System.Timers.Timer ();
			monitorConnection.Interval = 2000;
			monitorConnection.AutoReset = true;
			monitorConnection.Elapsed += new ElapsedEventHandler (monitor);
			monitorConnection.Enabled = true;
			monitorConnection.Start ();
		}

		private void monitor(object source, ElapsedEventArgs e) {
			if (iocpCli.isConnected) {
				Console.WriteLine ("Var: " + iocpCli.getAnalogInput (10).Value);
			} else {
				Console.WriteLine (“Not connected!”);
			}


		}
	}
}

```