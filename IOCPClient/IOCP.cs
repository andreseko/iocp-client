using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace IOCPClient
{
	public class IOCP
	{
		private string hostname;
		private string port;
		private TcpClient client;
		private Dictionary<int, int> inputs = new Dictionary<int, int>();
		private DateTime initSent = DateTime.MinValue;
		private StreamReader reader;
		private StreamWriter writer;
		private Thread readerThread;
		private Thread guardThread;
		private List<int> toMonitor;

		public IOCP(string hostname, int port = 8092)
		{
			this.hostname = hostname;
			this.port = port.ToString();
			guardThread = new Thread(new ThreadStart(guard));
			guardThread.IsBackground = true;
			guardThread.Start();
			_instance = this;
		}


		private void guard()
		{
			while (true)
			{
				if (client == null)
				{
					try
					{
						startup(new TcpClient(hostname, int.Parse(port)));
					}
					catch (Exception e) { 
						Console.WriteLine (e.Message);
					}
				}
				else if (!readerThread.IsAlive)
				{
					shutdown();
				}
				Thread.Sleep(1000);
			}
		}

		private void startup(TcpClient client)
		{
			this.client = client;
			readerThread = new Thread(new ThreadStart(readerLoop));
			readerThread.IsBackground = true;
			reader = new StreamReader(client.GetStream());
			writer = new StreamWriter(client.GetStream());
			readerThread.Start();
			sendInit();
		}

		public void shutdown()
		{
			client.Close();
			client = null;
			Thread.Sleep(20);
		}

		public void sendInit()
		{
			if (toMonitor != null)
			{
				StringBuilder builder = new StringBuilder();
				builder.Append("Arn.Inicio:");
				foreach (int num in toMonitor)
				{
					builder.Append(num).Append(":");
				}
				write(builder.ToString());
				initSent = DateTime.Now;
			}
		}

		public void setMapping(Dictionary<string, int> hardwareAddress)
		{
			toMonitor = new List<int>();
			foreach (KeyValuePair<string, int> address in hardwareAddress)
			{
				toMonitor.Add(address.Value);
			}
			hardwareReady = true;
			sendInit();
		}

		public void setOutput(int port, int value)
		{
			write(string.Concat(new object[] { "Arn.Resp:", port, "=", value }));
			Thread.Sleep(20);
		}

		public void setDisplay(int port, string value)
		{
			write(string.Concat(new object[] { "Arn.Resp:", port, "=", value }));
			Thread.Sleep(20);
		}

		private void readerLoop()
		{
			try
			{
				string str;
				Regex regex = new Regex(":([0-9]+)=(-?[0-9]+)");
				loopInputs:
					str = reader.ReadLine();

				if (str != null)
				{
					if (str.StartsWith("Arn.Resp:"))
					{
						foreach (Match match in regex.Matches(str))
						{
							int num;
							int num2;
							GroupCollection groups = match.Groups;
							if (int.TryParse(groups[1].Value, out num) && int.TryParse(groups[2].Value, out num2))
							{
								lock (inputs)
								{
									inputs[num] = num2;
									continue;
								}
							}
						}
					}
					goto loopInputs;
				}
			}
			catch (Exception) {}
		}

		public int? getAnalogInput(int port)
		{
			if (isConnected)
			{
				lock (inputs)
				{
					if (inputs.ContainsKey(port))
					{
						return new int?(inputs[port]);
					}
					if ((initSent != DateTime.MinValue) && (DateTime.Now.Subtract(initSent).TotalSeconds > 5.0))
					{
						return 0;
					}
				}
			}
			return null;
		}

		private void write(string s)
		{
			if (isConnected)
			{
				try
				{
					lock (writer)
					{
						writer.Write(s);
						writer.Write('\r');
						writer.Write('\n');
						writer.Flush();
					}
				}
				catch (Exception)
				{
					shutdown();
				}
			}
		}

		public bool isConnected
		{
			get { return this.client != null ? true : false; }
		}

		public bool hardwareReady {
			get;
			private set;
		}

		private IOCP _instance;
		public IOCP getInstance {
			get { return _instance; }
		}
	}
}

